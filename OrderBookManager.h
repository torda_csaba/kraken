//
// Created by tcsaba on 3/5/22.
//

#ifndef KRAKEN_ORDERBOOKMANAGER_H
#define KRAKEN_ORDERBOOKMANAGER_H

#include "DataConnector.h"
#include "UdpServer.h"
#include "transaction.h"

#include <thread>
#include <regex>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/lexical_cast.hpp>

template<typename E>
constexpr auto to_underlying(E e) noexcept {
    return static_cast<std::underlying_type_t<E>>(e);
}

template<typename T>
class OrderBookManager {
    template<class U>
    using Book = boost::multi_index::multi_index_container<
            NewOrder,
            boost::multi_index::indexed_by<
                    boost::multi_index::ordered_non_unique<
                            boost::multi_index::tag<struct price>,
                            boost::multi_index::member<NewOrder, int, &NewOrder::price>,
                            U
                    >
            >
    >;
    using SellBook = Book<std::less<int>>; /* top is lowest price */
    using BuyBook = Book<std::greater<int>>; /* top is lowest price */

    using container = typename T::DataContainer;
    std::shared_ptr<T> _dataConnector;
    container _data;
    std::thread _thread;

    enum class Side : char {
        Buy = 'B',
        Sell = 'S'
    } side;

    struct OrderBook {
        SellBook _sellBook;
        BuyBook _buyBook;
    };

    std::unordered_map<std::string, OrderBook> _orderBooks;

    class TOBChecker
    {
        const OrderBook& _orderBook;
        std::pair<int, int> _buyTOBBefore;
        std::pair<int, int> _sellTOBBefore;

        template<typename V>
        std::pair<int, int> getTOB(V& view){
            std::pair<int, int> ret{0,0};
            if (!view.empty()) {
                ret.first = view.begin()->price;
                for(auto it = view.begin(); it != view.end() && ret.first == it->price; ++ it)
                {
                    ret.second += it->qty;
                }
            }
            return ret;
        }

    public:
        explicit TOBChecker(const OrderBook &orderBook) : _orderBook{orderBook}, _buyTOBBefore{0, 0}, _sellTOBBefore{0, 0}
        {
            auto &buyView = _orderBook._buyBook.template get<price>();
            auto &sellView = _orderBook._sellBook.template get<price>();
            _buyTOBBefore = getTOB(buyView);
            _sellTOBBefore = getTOB(sellView);            
        }
        ~TOBChecker()
        {
            auto &buyView = _orderBook._buyBook.template get<price>();
            auto &sellView = _orderBook._sellBook.template get<price>();

            const std::pair<int, int> zero{0,0};

            const auto buyTOBAfter = getTOB(buyView);
            const auto sellTOBAfter = getTOB(sellView);  

            if (buyTOBAfter != _buyTOBBefore) {
                if (buyTOBAfter == zero )
                {
                    std::cout << "B, " << to_underlying(Side::Buy) << ", -, -" << std::endl;
                }
                else
                {
                    std::cout << "B, " << to_underlying(Side::Buy) << ", " << buyTOBAfter.first << ", " << buyTOBAfter.second << std::endl;
                }
            }
            if (sellTOBAfter != _sellTOBBefore) {
                if (sellTOBAfter == zero )
                {
                    std::cout << "B, " << to_underlying(Side::Sell) << ", -, -" << std::endl;
                }
                else
                {
                    std::cout << "B, " << to_underlying(Side::Sell) << ", " << sellTOBAfter.first << ", " << sellTOBAfter.second << std::endl;
                }
            }
        }
        
    };

public:
    OrderBookManager(std::shared_ptr<T> &dataConnector) :
            _dataConnector{dataConnector},
            _thread{&OrderBookManager<T>::processTransactions,
                    this} {
    }

    ~OrderBookManager() {
        _thread.join();
    }

    void publishTrade(int userIdBuy, int userOrderIdBuy, int userIdSell, int userOrderIdSell, int price, int quantity) {
        std::cout << "T, " << userIdBuy << ", " << userOrderIdBuy << ", " << userIdSell << ", " << userOrderIdSell
                  << ", " << price << ", " << quantity << std::endl;
    }

    void matchBuyOrder(NewOrder &order, OrderBook &orderBook) {
        auto &view = orderBook._sellBook.template get<price>();

        auto itBeginRemove = view.end();
        auto itEndRemove = view.end();

        for (auto sellItemIt = view.begin(); sellItemIt != view.end() && order.qty > 0; ++sellItemIt) {
            if (order.price == 0 || sellItemIt->price <= order.price) {
                if (sellItemIt->qty <= order.qty) {
                    publishTrade(order.user, order.userOrderId, sellItemIt->user, sellItemIt->userOrderId,
                                 sellItemIt->price, sellItemIt->qty);
                    order.qty -= sellItemIt->qty;
                    if (itBeginRemove == view.end()) {
                        itBeginRemove = itEndRemove = sellItemIt;
                    } else {
                        itEndRemove = sellItemIt;
                    }
                } else {
                    publishTrade(order.user, order.userOrderId, sellItemIt->user, sellItemIt->userOrderId,
                                 sellItemIt->price, order.qty);
                    view.template modify(sellItemIt, [qty = order.qty](auto &item) { item.qty -= qty; });
                    order.qty = 0;
                }
            } else {
                break;
            }
        }

        if (itBeginRemove != view.end()) {
            view.erase(itBeginRemove, ++itEndRemove);
        }

        if (order.qty > 0 && order.price != 0) /* join the book if not market order (fill-and-kill) */
        {
            orderBook._buyBook.template emplace(order);
        }
    }

    void matchSellOrder(NewOrder &order, OrderBook &orderBook) {
        auto &view = orderBook._buyBook.template get<price>();

        auto itBeginRemove = view.end();
        auto itEndRemove = view.end();

        for (auto buyItemIt = view.begin(); buyItemIt != view.end() && order.qty > 0; ++buyItemIt) {
            if (order.price == 0 || buyItemIt->price >= order.price) {
                if (buyItemIt->qty <= order.qty) {
                    publishTrade(buyItemIt->user, buyItemIt->userOrderId, order.user, order.userOrderId,
                                 buyItemIt->price, buyItemIt->qty);
                    order.qty -= buyItemIt->qty;
                    if (itBeginRemove == view.end()) {
                        itBeginRemove = itEndRemove = buyItemIt;
                    } else {
                        itEndRemove = buyItemIt;
                    }
                } else {
                    publishTrade(buyItemIt->user, buyItemIt->userOrderId, order.user, order.userOrderId,
                                 buyItemIt->price, order.qty);
                    view.template modify(buyItemIt, [qty = order.qty](auto &item) { item.qty -= qty; });
                    order.qty = 0;
                }
            } else {
                break;
            }
        }

        if (itBeginRemove != view.end()) {
            view.erase(itBeginRemove, ++itEndRemove);
        }

        if (order.qty > 0 && order.price != 0) /* join the book if not market order (fill-and-kill) */
        {
            orderBook._sellBook.template emplace(order);
        }
    }

    void processNewOrder(std::string symbol, Side side, NewOrder order) {
        OrderBook &orderBook = _orderBooks[std::move(symbol)];
        auto tobGuard = TOBChecker{orderBook};
        std::cout << "A, " << order.user << ", " << order.userOrderId << std::endl;
        if (side == Side::Buy) {
            matchBuyOrder(order, orderBook);
        } else /* if (side == Side::Sell) */
        {
            matchSellOrder(order, orderBook);
        }
    }

    void processCancelOrder(const CancelOrder& cancelOrder)
    {
        std::cout << "A, " << cancelOrder.user << ", " << cancelOrder.userOrderId << std::endl;
        for (auto p : _orderBooks)
        {
            for (auto it = p.second._sellBook.begin(); it != p.second._sellBook.end(); ++ it)
            {
                if (it->user == cancelOrder.user && it->userOrderId == cancelOrder.userOrderId)
                {
                    auto tobGuard = TOBChecker{p.second};
                    p.second._sellBook.erase(it);
                    return;
                }
            }
            for (auto it = p.second._buyBook.begin(); it != p.second._buyBook.end(); ++ it)
            {
                if (it->user == cancelOrder.user && it->userOrderId == cancelOrder.userOrderId)
                {
                    auto tobGuard = TOBChecker{p.second};
                    p.second._buyBook.erase(it);
                    return;
                }
            }
        }
    }

    void processTransaction(std::string_view transactionStr) {
        std::regex newOrderRE{"^N,\\s([0-9]+),\\s(.*?),\\s([0-9]+),\\s([0-9]+),\\s([B,S]),\\s([0-9]+)$"};
        std::regex cancelOrderRE{"^C,\\s([0-9]+),\\s([0-9]+)$"};
        std::string_view flushOrderBook{"F"};
        std::match_results<std::string_view::const_iterator> match;

        if (transactionStr == flushOrderBook) {
            _orderBooks.clear();
        } else if (std::regex_match(transactionStr.begin(), transactionStr.end(), match, newOrderRE)) {
            NewOrder newOrder{};
            newOrder.user = boost::lexical_cast<int>(match[1]);
            newOrder.price = boost::lexical_cast<int>(match[3]);
            newOrder.qty = boost::lexical_cast<int>(match[4]);
            newOrder.userOrderId = boost::lexical_cast<int>(match[6]);
            processNewOrder(match[2], Side{*(match[5].first)},
                            newOrder);    // match[2] ‘const std::__cxx11::sub_match<const char*>’
        } else if (std::regex_match(transactionStr.begin(), transactionStr.end(), match, cancelOrderRE)) {
            CancelOrder cancelOrder{};
            cancelOrder.user = boost::lexical_cast<int>(match[1]);
            cancelOrder.userOrderId = boost::lexical_cast<int>(match[2]);
            processCancelOrder(cancelOrder);
        } else {
            throw std::string{"invalid message"};
        }
    }

    void processTransactions() {
        do {
            _dataConnector->waitForData(_data);
            for (const auto &buffer: _data) {
                if (buffer.size() == 0) {
                    continue;
                }
                size_t start = 0;
                size_t end = 0;

                while (start < buffer.size()) {
                    for (size_t index = start; index < buffer.size(); ++index) {
                        if (buffer[index] != '\n' && buffer[index] != '\r') {
                            ++end;
                        } else {
                            break;
                        }
                    }
                    if (start == end) {
                        start = ++end;
                        continue;
                    }
                    std::string_view curr{&buffer[start], end - start};
                    try {
                        processTransaction(curr);
                    } catch (const std::string &e) {
                        std::cerr << "[OrderBookManager::processTransactions] exception caught: " << e;
                    } catch (const boost::bad_lexical_cast &e) {
                        std::cerr << "[OrderBookManager::processTransactions] exception caught: " << e.what();
                    }
                    start = ++end;
                }
            }
        } while (!_data.empty());
    }
};

#endif //KRAKEN_ORDERBOOKMANAGER_H
