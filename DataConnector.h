//
// Created by tcsaba on 3/5/22.
//

#ifndef KRAKEN_DATACONNECTOR_H
#define KRAKEN_DATACONNECTOR_H

#include <boost/container/small_vector.hpp>
#include <boost/container/static_vector.hpp>
#include <mutex>
#include <condition_variable>

template<std::size_t MaxSize>
class DataConnector {
public:
    using DataContainer = boost::container::small_vector<boost::container::static_vector<char, MaxSize>, 3>;
    DataConnector() = default;

    void provideData(const std::array<char, MaxSize>& data, std::size_t size)
    {
        std::lock_guard<std::mutex> lock{cv_m};
        if (!alive)
        {
            return;
        }
        auto endIt = data.begin();
        std::advance(endIt, size);
        _dataContainer.emplace_back (data.begin(), endIt);
        _cv.notify_one();
    }

    void waitForData(DataContainer& dst)
    {
        std::unique_lock<std::mutex> lock{cv_m};
        _cv.wait(lock, [this](){
            return !_dataContainer.empty() || !alive;
        });
        dst = std::move(_dataContainer);
    }

    void disable()
    {
        std::lock_guard<std::mutex> lock{cv_m};
        alive = false;
        _dataContainer.clear();
        _cv.notify_one();
    }

private:
    std::condition_variable _cv;
    std::mutex cv_m;
    bool alive{true};
    DataContainer _dataContainer;
};


#endif //KRAKEN_DATACONNECTOR_H
