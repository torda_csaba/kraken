//
// Created by tcsaba on 3/5/22.
//
#include <iostream>
#include "UdpServer.h"

UdpServer::UdpServer(uint16_t port, std::shared_ptr<DataConnector<MaxBufferSize>>& dataConnector)
        : _dataConnector{dataConnector}, _iocontext{}, _endpoint{udp::endpoint(udp::v4(), port)}, _socket(_iocontext, _endpoint),
          _thread{&UdpServer::run, this} {
    startReceive();
}

UdpServer::~UdpServer() {
    _iocontext.stop();
    _thread.join();
}

void UdpServer::run() {
    while (true) {
        try {
            auto work = boost::asio::make_work_guard(_iocontext);
            if (_iocontext.run() == 0u) {
                break;
            }
        } catch (const std::exception &e) {
            std::cerr << "Server network exception: " << e.what();
        } catch (...) {
            std::cerr << "Unknown exception in server network thread";
        }
    }
}

void UdpServer::startReceive() {
    _socket.async_receive_from(boost::asio::buffer(_recvBuffer), _endpoint,
                               [this](boost::system::error_code ec, size_t xfer) { handleReceive(ec, xfer); });
}

void UdpServer::handleReceive(const boost::system::error_code &error, std::size_t bytes_transferred) {
    if (!error || error == boost::asio::error::message_size) {
        _dataConnector->provideData(_recvBuffer, bytes_transferred);
        auto message = std::make_shared<std::string>("Hello, World\n");
        _socket.async_send_to(boost::asio::buffer(*message), _endpoint,
                              [this, message = message](const boost::system::error_code &ec,
                                                        std::size_t bytes_transferred) {
                                  handleSend(message, ec, bytes_transferred);
                              });
    }
}

void UdpServer::handleSend(std::shared_ptr<std::string> message, const boost::system::error_code &ec,
                           std::size_t bytes_transferred) {
    startReceive();
}
