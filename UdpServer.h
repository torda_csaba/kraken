//
// Created by tcsaba on 3/5/22.
//

#ifndef KRAKEN_UDPSERVER_H
#define KRAKEN_UDPSERVER_H

#include <thread>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include "DataConnector.h"

using boost::asio::ip::udp;
using namespace boost::placeholders;

class UdpServer {
public:
    static constexpr size_t MaxBufferSize = 65535;
    UdpServer(uint16_t port, std::shared_ptr<DataConnector<MaxBufferSize>>& dataConnector);
    ~UdpServer();
    void run();
    void stop();

private:
    void startReceive();
    void handleReceive(const boost::system::error_code &error,
                       std::size_t bytes_transferred);
    void handleSend(std::shared_ptr<std::string> message,
                    const boost::system::error_code &ec,
                    std::size_t bytes_transferred);

    std::shared_ptr<DataConnector<MaxBufferSize>> _dataConnector;
    boost::asio::io_context _iocontext;
    udp::endpoint _endpoint;
    udp::socket _socket;
    std::array<char, MaxBufferSize> _recvBuffer;
    std::thread _thread;
};


#endif //KRAKEN_UDPSERVER_H
