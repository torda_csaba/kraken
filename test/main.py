import socket
from time import sleep
import re


if __name__ == "__main__":
    udp_ip = "localhost"
    udp_port = 1111
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    with open("inputFile.csv") as fp:
        for line in fp:
            line = line.strip()
            if re.search('#scenario', line, re.IGNORECASE):
                print(line)
                continue
            if line.startswith("#") or len(line) == 0:
                continue
            if line:
                b = bytes(line, "utf-8")
                sock.sendto(b, (udp_ip, udp_port))
                sleep(0.05)
    exit(0)