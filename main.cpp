#include <string>
#include <iostream>

#include "UdpServer.h"
#include "DataConnector.h"
#include "OrderBookManager.h"

int main() {
    try {
        auto dataConnector = std::make_shared<DataConnector<UdpServer::MaxBufferSize>>();
        UdpServer server{1111, dataConnector};
        OrderBookManager<DataConnector<UdpServer::MaxBufferSize>> orderBook{dataConnector};
        std::string cmdLine;
        std::cin >> cmdLine;
        dataConnector->disable();
    } catch (const std::exception &ex) {
        std::cerr << ex.what() << std::endl;
    } catch (...) {
        std::cerr << "Unknown exception";
    }
    return 0;
}