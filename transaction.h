//
// Created by tcsaba on 3/4/22.
//

#ifndef KRAKEN_TRANSACTION_H
#define KRAKEN_TRANSACTION_H

#include <tuple>
#include <type_traits>

struct NewOrder {
    int user;
    int price;
    int qty;
    int userOrderId;

    friend bool operator!=(const NewOrder &lhs, const NewOrder &rhs) {
        return std::tie(lhs.user, lhs.price, lhs.qty, lhs.userOrderId) !=
               std::tie(rhs.user, rhs.price, rhs.qty, rhs.userOrderId);
    }
};

static_assert(std::is_pod_v<NewOrder>);

struct CancelOrder {
    int user;
    int userOrderId;
};

#endif //KRAKEN_TRANSACTION_H
